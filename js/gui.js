function createAddSmallCylinderButton(){
  var button = BABYLON.GUI.Button.CreateImageOnlyButton(
    "addCylinderSmall",
    "img/smallCylinder_3.png"
  );
  button.width = "200px";
  button.height = "150px";
  button.color = "transparent";

  return button;
}

function createAddMediumCylinderButton(){
      var button = BABYLON.GUI.Button.CreateImageOnlyButton(
        "addCylinderMedium",
        "img/mediumCylinder_3.png"
      );
      button.width = "200px";
      button.height = "150px";
      button.color = "transparent";

      return button;
}

function createAddLargeCylinderButton(){
  var button = BABYLON.GUI.Button.CreateImageOnlyButton(
    "addCylinderLarge",
    "img/largeCylinder_3.png"
  );
  button.width = "200px";
  button.height = "150px";
  button.color = "transparent";

  return button;
}

function createDeleteCylinderButton(){
    var button = BABYLON.GUI.Button.CreateSimpleButton(
        "deleteCylinder",
        "Löschen"
      );
      button.width = "120px";
      button.height = "50px";
      button.color = "white";
      //button.background = "grey";
      button.isVisible = false;
      return button;
}

function createColorBlueButton(){
  var button = BABYLON.GUI.Button.CreateSimpleButton("blue", "");
  button.width = "50px";
  button.height = "50px";
  button.color = "grey";
  button.background = "blue";
  button.isVisible = false;
  return button;
}

function createColorSilverButton(){
  var button = BABYLON.GUI.Button.CreateSimpleButton("silver", "");
  button.width = "50px";
  button.height = "50px";
  button.color = "grey";
  button.background = "silver";
  button.isVisible = false;
  return button;
}