function createPlateMaterial() {
  var plateMaterial = new BABYLON.PBRMetallicRoughnessMaterial(
    "plateMaterial",
    scene
  );
  plateMaterial.baseColor = new BABYLON.Color3(0.17, 0.17, 0.17);
  plateMaterial.metallic = 0.6;
  plateMaterial.roughness = 0.2;

  return plateMaterial;
}

function createCylinderMaterial(){
    var cylinderMaterial = new BABYLON.PBRMetallicRoughnessMaterial(
        "cylinderMaterial",
        scene
      );
      cylinderMaterial.baseColor = new BABYLON.Color3(0.85, 0.85, 0.85);
      cylinderMaterial.metallic = 0.2;
      cylinderMaterial.roughness = 0.5;

      return cylinderMaterial;
}