var addSmallCylinderButtonListener = function () {
  if (currentStatus != StatusEnum.PLACING_CYLINDER) {
    //selectedCylinder = cylinderMesh.createInstance("cylinder" + (cylinderCount++));
    selectedCylinder = smallCylinderMesh.clone("cylinder" + cylinderCount++);
    selectedCylinder.cylinderHeight = smallCylinderHeight;
    setupCylinder();

    currentStatus = StatusEnum.PLACING_CYLINDER;
  }
};

var addMediumCylinderButtonListener = function () {
  if (currentStatus != StatusEnum.PLACING_CYLINDER) {
    //selectedCylinder = cylinderMesh.createInstance("cylinder" + (cylinderCount++));
    selectedCylinder = mediumCylinderMesh.clone("cylinder" + cylinderCount++);
    selectedCylinder.cylinderHeight = mediumCylinderHeight;
    setupCylinder();

    currentStatus = StatusEnum.PLACING_CYLINDER;
  }
};

var addLargeCylinderButtonListener = function () {
  if (currentStatus != StatusEnum.PLACING_CYLINDER) {
    //selectedCylinder = cylinderMesh.createInstance("cylinder" + (cylinderCount++));
    selectedCylinder = largeCylinderMesh.clone("cylinder" + cylinderCount++);
    selectedCylinder.cylinderHeight = largeCylinderMeshHeight;
    setupCylinder();

    currentStatus = StatusEnum.PLACING_CYLINDER;
  }
};

function setupCylinder() {
  selectedCylinder.isVisible = true;
  selectedCylinder.material = cylinderMaterial.clone();
  selectedCylinder.position = new BABYLON.Vector3(-300, 100, 0);

  highlightLayer.removeAllMeshes();
  highlightLayer.addMesh(selectedCylinder, BABYLON.Color3.Red());
  selectedCylinder.addBehavior(createPointerDragBehavior());

  selectedCylinder.actionManager = new BABYLON.ActionManager(scene);
  selectedCylinder.actionManager.registerAction(onCylinderClickedAction);

  deleteCylinderButton.isVisible = true;
  blueColorButton.isVisible = true;
  silverColorButton.isVisible = true;

  currentStatus = StatusEnum.PLACING_CYLINDER;
}

var onCylinderClickedAction = new BABYLON.ExecuteCodeAction(
  {
    trigger: BABYLON.ActionManager.OnPickDownTrigger,
  },
  function (evt) {
    if (currentStatus != StatusEnum.PLACING_CYLINDER) {
      highlightLayer.removeAllMeshes();
      if (evt.source != selectedCylinder) {
        highlightLayer.addMesh(evt.source, BABYLON.Color3.Green());
        selectedCylinder = evt.source;
        deleteCylinderButton.isVisible = true;
        blueColorButton.isVisible = true;
        silverColorButton.isVisible = true;
        currentStatus = StatusEnum.CYLINDER_SELECTED;
      } else {
        selectedCylinder = null;
        deleteCylinderButton.isVisible = false;
        blueColorButton.isVisible = false;
        silverColorButton.isVisible = false;
        currentStatus = StatusEnum.NONE;
      }
    }
  }
);

var deleteCylinderListener = function () {
  if (currentStatus == StatusEnum.CYLINDER_SELECTED) {
    var cylinderStack;
    if (selectedCylinder.stack == 0) {
      cylinderStack = leftCylinders;
    } else {
      cylinderStack = rightCylinders;
    }

    const index = cylinderStack.indexOf(selectedCylinder);

    if (index > -1) {
      cylinderStack.cylinderHeight -= selectedCylinder.cylinderHeight;
      for (var i = index + 1; i < cylinderStack.length; i++) {
        cylinderStack[i].position.y -= selectedCylinder.cylinderHeight;
      }
      cylinderStack.splice(index, 1);
      selectedCylinder.dispose();
      selectedCylinder = null;
      deleteCylinderButton.isVisible = false;
      blueColorButton.isVisible = false;
      silverColorButton.isVisible = false;
    } else {
      return;
    }
  } else if (currentStatus == StatusEnum.PLACING_CYLINDER) {
    selectedCylinder.dispose();
    currentStatus = StatusEnum.NONE;
    deleteCylinderButton.isVisible = false;
    blueColorButton.isVisible = false;
    silverColorButton.isVisible = false;
  }
};

function createPointerDragBehavior() {
  var pointerDragBehavior = new BABYLON.PointerDragBehavior({
    dragPlaneNormal: new BABYLON.Vector3(0, 1, 0),
  });

  pointerDragBehavior.onDragStartObservable.add((event) => {});
  pointerDragBehavior.onDragObservable.add((event) => {
    if (currentStatus == StatusEnum.PLACING_CYLINDER) {
      snapOnDemand(pointerDragBehavior.attachedNode);
    }
  });
  pointerDragBehavior.onDragEndObservable.add((event) => {
    if (currentStatus == StatusEnum.PLACING_CYLINDER) {
      snapOnDemand(pointerDragBehavior.attachedNode);
      if (placeCylinder(pointerDragBehavior.attachedNode)) {
        pointerDragBehavior.enabled = false;
        deleteCylinderButton.isVisible = false;
        blueColorButton.isVisible = false;
        silverColorButton.isVisible = false;
        selectedCylinder = null;
        currentStatus = StatusEnum.NONE;
      }
    }
  });

  return pointerDragBehavior;
}

function snapOnDemand(cylinder) {
  if (currentStatus == StatusEnum.PLACING_CYLINDER) {
    var x = cylinder.position.x;
    var y = cylinder.position.y;

    var leftDistance = BABYLON.Vector2.Distance(
      new BABYLON.Vector2(x, y),
      new BABYLON.Vector2(xLeft, leftCylinders.cylinderHeight)
    );
    var rightDistance = BABYLON.Vector2.Distance(
      new BABYLON.Vector2(x, y),
      new BABYLON.Vector2(xRight, rightCylinders.cylinderHeight)
    );

    highlightLayer.removeAllMeshes();
    if (leftDistance < 10) {
      cylinder.position.x = xLeft;
      cylinder.position.y = leftCylinders.cylinderHeight;
      highlightLayer.addMesh(selectedCylinder, BABYLON.Color3.Green());
    } else if (rightDistance < 10) {
      cylinder.position.x = xRight;
      cylinder.position.y = rightCylinders.cylinderHeight;
      highlightLayer.addMesh(cylinder, BABYLON.Color3.Green());
    } else {
      highlightLayer.addMesh(cylinder, BABYLON.Color3.Red());
    }
  }
}

function placeCylinder(cylinder) {
  if (currentStatus == StatusEnum.PLACING_CYLINDER) {
    if (cylinder.position.x == xRight) {
      cylinder.stack = 1;
      rightCylinders.push(cylinder);
      rightCylinders.cylinderHeight += cylinder.cylinderHeight;
    } else if (cylinder.position.x == xLeft) {
      cylinder.stack = 0;
      leftCylinders.push(cylinder);
      leftCylinders.cylinderHeight += cylinder.cylinderHeight;
    } else {
      //Keine gültige Position
      return false;
    }
    highlightLayer.removeAllMeshes();
    return true;
  }
  return false;
}

function changeColor(color){
  if(selectedCylinder){
    selectedCylinder.material.baseColor = color;
  }
}