// Globale Variablen

//Allgemeine Szene-Variablen
var scene;
var engine;
var canvas;
var highlightLayer;
var light;
var camera;
var assetLoader;

//Eigene Assets
var plateMesh;
var smallCylinderMesh;
var mediumCylinderMesh;
var largeCylinderMesh;
var plateMaterial;
var cylinderMaterial;

//GUI
var deleteCylinderButton;
var blueColorButton;
var silverColorButton;

//Konstanten
const xRight = 70;
const xLeft = -80;
const smallCylinderHeight = 23;
const mediumCylinderHeight = 32;
const largeCylinderMeshHeight = 41;

//Logik
var leftCylinders = [];
var rightCylinders = [];
leftCylinders.cylinderHeight = 0;
rightCylinders.cylinderHeight = 0;
var cylinderCount = 0;

var selectedCylinder;

var StatusEnum = Object.freeze ({ NONE: 1, PLACING_CYLINDER: 2, CYLINDER_SELECTED: 3});
var currentStatus = StatusEnum.NONE;

function main() {
  canvas = document.getElementById("renderCanvas");
  engine = new BABYLON.Engine(canvas, true);

  var scene = createScene();

  engine.runRenderLoop(function () {
    scene.render();
  });

  window.addEventListener("resize", function () {
    engine.resize();
  });
}

function createScene() {
  scene = new BABYLON.Scene(engine);
  highlightLayer = new BABYLON.HighlightLayer("hl1", scene);
  scene.ambientColor = new BABYLON.Color3(1, 1, 1);
  //scene.clearColor = new BABYLON.Color3(0.8, 0.8, 0.8);
  light = new BABYLON.HemisphericLight(
    "omni",
    new BABYLON.Vector3(0, 100, 0),
    scene
  );


  camera = new BABYLON.ArcRotateCamera(
    "camera",
    -Math.PI / 2,
    BABYLON.Tools.ToRadians(67),
    700,
    new BABYLON.Vector3(0, 0, 0),
    scene
  );
  camera.attachControl(canvas);

  plateMaterial = createPlateMaterial();
  cylinderMaterial = createCylinderMaterial();

  assetLoader = new BABYLON.AssetsManager(scene);
  loadSmallCylinderMesh();
  loadMediumCylinderMesh();
  loadLargeCylinderMesh();
  loadPlateMesh();
  assetLoader.load();

  var advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI(
    "gui"
  );
  var panel = new BABYLON.GUI.StackPanel();
  var addSmallCylinderButton = createAddSmallCylinderButton();
  var addMediumCylinderButton = createAddMediumCylinderButton();
  var addLargeCylinderButton = createAddLargeCylinderButton();
  deleteCylinderButton = createDeleteCylinderButton();
  
  panel.addControl(addLargeCylinderButton);
  panel.addControl(addMediumCylinderButton);
  panel.addControl(addSmallCylinderButton);

  panel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER;
  panel.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
  panel.isVertical = true;
  panel.adaptWidthToChildren=true;
  advancedTexture.addControl(panel);


  addSmallCylinderButton.onPointerClickObservable.add(addSmallCylinderButtonListener);
  addMediumCylinderButton.onPointerClickObservable.add(addMediumCylinderButtonListener);
  addLargeCylinderButton.onPointerClickObservable.add(addLargeCylinderButtonListener);
  deleteCylinderButton.onPointerClickObservable.add(deleteCylinderListener);


  blueColorButton = createColorBlueButton();
  blueColorButton.onPointerClickObservable.add(function(){
    changeColor(new BABYLON.Vector3(30/255, 144/255,255/255));
  })
  silverColorButton = createColorSilverButton();
  silverColorButton.onPointerClickObservable.add(function(){
    changeColor(new BABYLON.Vector3(1, 1, 1));
  })
  var panel2 = new BABYLON.GUI.StackPanel();
  panel2.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_BOTTOM;
  panel2.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER;
  panel2.isVertical = false;
  
  panel2.addControl(deleteCylinderButton);
  panel2.addControl(blueColorButton);
  panel2.addControl(silverColorButton);
  panel2.adaptHeightToChildren=true;
  advancedTexture.addControl(panel2);

  return scene;
}

