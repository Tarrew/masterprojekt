function loadSmallCylinderMesh() {
  var cylinder = assetLoader.addMeshTask(
    "",
    "",
    "assets/",
    "smallCylinder.obj"
  );

  cylinder.onSuccess = function () {
    smallCylinderMesh = cylinder.loadedMeshes[0];
    smallCylinderMesh.material = cylinderMaterial;
    smallCylinderMesh.isVisible = false;
    smallCylinderMesh.rotation.x = -Math.PI / 2;
    smallCylinderMesh.position = new BABYLON.Vector3(xLeft, 0, 0);
  };
}

function loadMediumCylinderMesh() {
  var cylinder = assetLoader.addMeshTask(
    "",
    "",
    "assets/",
    "mediumCylinder.obj"
  );

  cylinder.onSuccess = function () {
    mediumCylinderMesh = cylinder.loadedMeshes[0];
    mediumCylinderMesh.material = cylinderMaterial;
    mediumCylinderMesh.isVisible = false;
    mediumCylinderMesh.rotation.x = -Math.PI / 2;
    mediumCylinderMesh.position = new BABYLON.Vector3(xLeft, 0, 0);
  };
}

function loadLargeCylinderMesh() {
  var cylinder = assetLoader.addMeshTask(
    "",
    "",
    "assets/",
    "largeCylinder.obj"
  );

  cylinder.onSuccess = function () {
    largeCylinderMesh = cylinder.loadedMeshes[0];
    largeCylinderMesh.material = cylinderMaterial;
    largeCylinderMesh.isVisible = false;
    largeCylinderMesh.rotation.x = -Math.PI / 2;
    largeCylinderMesh.position = new BABYLON.Vector3(xLeft, 0, 0);
  };
}

function loadPlateMesh() {
  var plate = assetLoader.addMeshTask(
    "",
    "",
    "assets/",
    "plate.obj"
  );
  plate.onSuccess = function () {
    plateMesh = plate.loadedMeshes[0];
    plateMesh.setPositionWithLocalVector(new BABYLON.Vector3(-80, 0, 0));
    plateMesh.rotation.x = -Math.PI / 2;
    plateMesh.material = plateMaterial;
  };
}
